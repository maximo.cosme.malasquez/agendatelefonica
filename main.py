from conexionMongo import MongoDB
from conexionMySql import MYSQL

print("OPCION 1: Mostrar base de datos  según MongoDB")
print("OPCION 2: Mostrar base de datos MYSQL")
print("")
opcion = input("Ingrese una opción: ")

if opcion == "1":
    conexionMongo = MongoDB()
    conexionMongo.listar('')
else:
    conexionMySql = MYSQL()
    conexionMySql.listar("")
